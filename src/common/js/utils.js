/**
 * 工具方法
 * author Cobbler 417663733@qq.com
 * create 2018-05-03
 */
import FormData from 'form-data'

/**
 * 取URL传参
 * @param name
 * @returns {*}
 */
export function getQueryString (name) {
  const reg = new RegExp(`(^|&)${name}=([^&]*)(&|$)`)
  const str = window.location.search.substr(1).match(reg)
  if (str !== null) {
    return decodeURI(str[2])
  }
  return null
}

export function objToFormData (obj) {
  const form = new FormData()

  for (const key in obj) {
    if ({}.hasOwnProperty.call(obj, key)) {
      form.append(key, obj[key])
    }
  }
  return form
}

/**
 * 延迟执行
 * @param ms
 * @returns {Promise<any>}
 */
export function timeout (ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms)
  })
}

/**
 * 获取位置
 * @param el
 * @returns {{top: number, left: number, width: number, height: number}}
 */
export function getRect (el) {
  if (el instanceof window.SVGElement) {
    let rect = el.getBoundingClientRect()
    return {
      top: rect.top,
      left: rect.left,
      width: rect.width,
      height: rect.height
    }
  } else {
    return {
      top: el.offsetTop,
      left: el.offsetLeft,
      width: el.offsetWidth,
      height: el.offsetHeight
    }
  }
}
