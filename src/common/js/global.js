import 'babel-polyfill'
import './flexible'
import './eruda'
import 'iview/dist/styles/iview.css'
import '@/common/styles/index.styl'
import fastclick from 'fastclick'

fastclick.attach(document.body)
