/**
 * $ 操作方法
 * author Cobbler 417663733@qq.com
 * create 2018-05-03
 */

/**
 * 是否是安卓系统
 * @type {boolean}
 */
export const ISANDROID = (/android/gi).test(navigator.appVersion)

/**
 * 是否是iOS系统
 * @type {boolean}
 */
export const ISIOS = (/iPhone|iPad/gi).test(navigator.appVersion)

/**
 * 去除字符串首尾空格
 * @param str
 * @returns {*}
 */
export function trim (str) {
  if (String.prototype.trim) {
    return str === null ? '' : String.prototype.trim.call(str)
  } else {
    return str.replace(/(^\s*)|(\s*$)/g, '')
  }
}

/**
 * 去除字符串中所有空格
 * @param str
 * @returns {string | void | *}
 */
export function trimAll (str) {
  return str.replace(/\s*/g, '')
}
