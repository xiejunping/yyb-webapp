import 'common/js/global'
import Vue from 'vue'
import App from './App'
import VueLazyload from 'vue-lazyload'
import loading from 'assets/pic.png'

Vue.config.productionTip = false
Vue.use(VueLazyload, { loading })

/* eslint-disable no-new */
new Vue({
  el: '#app',
  render: h => h(App)
})
