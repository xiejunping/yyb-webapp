import ReqClient from './request.class'
import {handleError} from './handle'

export async function getDetailByTeacher (params) {
  const Req = new ReqClient({
    url: '/phone/v2/homework/getDetailByTeacher',
    data: params,
    method: 'GET'
  })
  try {
    return await Req.reqData()
  } catch (e) {
    handleError(e)
  }
}
