import ReqClient from './request.class'
import {handleError} from './handle'

export async function getMyInfo (userId) {
  const Req = new ReqClient({
    url: '/phone/v2/login/getMyInfo',
    data: {userId}
  })
  try {
    return await Req.reqData()
  } catch (e) {
    handleError(e)
  }
}
