import ReqClient from './request.class'
import {handleError} from './handle'

/**
 * 请求视频列表数据
 * @param params
 * @returns {Promise<*>}
 */
export async function getAdvData (puserType) {
  const Req = new ReqClient({
    url: '/phone/v2/common/getAdvData',
    data: {puserType}
  })
  try {
    return await Req.reqData()
  } catch (e) {
    handleError(e)
  }
}

export async function savePlayAction (entryId, puserId) {
  const Req = new ReqClient({
    url: '/phone/exp/savePlayAction',
    data: {
      entryId,
      puserId
    }
  })
  try {
    return await Req.reqData()
  } catch (e) {
    handleError(e)
  }
}
