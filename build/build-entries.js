const path = require('path')
const fs = require('fs')
const glob = require('glob')
const utils = require('./utils')

let buildEntries = {}

/*获取所有模块的文件夹名*/
const modules = fs.readdirSync(path.join(utils.resolve('src'),'modules'))

for (let moduleName of modules) {
  buildEntries[moduleName] = path.join(utils.resolve('src'),'modules',moduleName,'main.js')
}

exports.buildEntries = buildEntries

exports.pages = ((globalPath)=>{
  let htmlFiles = {}
  let pageName

  glob.sync(globalPath).forEach((pagePath) => {
    let basename = path.basename(pagePath, path.extname(pagePath))
    pageName = basename
    htmlFiles[pageName] = {}
    htmlFiles[pageName]['chunk'] = basename
    htmlFiles[pageName]['path'] = pagePath
  })
  return htmlFiles
})(utils.resolve('src')+'/modules/**/*.html')
