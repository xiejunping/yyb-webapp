# yyb-webapp

> A Vue.js project

## Build Setup

``` bash
// 安装依赖包
# install dependencies
npm install

// 开发调试命令
# serve with hot reload at localhost:8080
npm run dev

// 产出静态文件
# build for production with minification
npm run build

// 产出静态文件本地服务浏览
npm run http

// 产出静态文件 + 分析
# build for production and view the bundle analyzer report
npm run build --report

```
### 放入后台banner 管理地址
innerBrowser:http://p889c49de.bkt.clouddn.com/video/video.html?userId=0e674be49a5e4feeab1d15d9a159e9a3&eruda=true&isAutoScreen=true

### 兼容安卓webview 版本
innerBrowser:http://p889c49de.bkt.clouddn.com/video/h5video.html?userId=0e674be49a5e4feeab1d15d9a159e9a3&eruda=true&isAutoScreen=true

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
